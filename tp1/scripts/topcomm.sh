#!/bin/bash
HISTFILE=~/.bash_history
set -o history
history | awk '{print $2}' | sort | uniq -c | sort -nr | head -$1

# la commande sur history fait dans l'ordre:
# - on sélectionne la deuxième colonne de la commande history
# - on range par ordre alphabétique
# - on regroupe les mêmes occurences sous une seule avec le nombre d'occurence à gauche
# - sort avec l'option -n permet de trier par le nombre d'occurences, et -r pour inverser la liste
# - enfin head -$1 signifie qu'on le prend que le nombre de ligne passé en argument (ici 10 par exemple)
