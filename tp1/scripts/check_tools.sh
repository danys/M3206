#!/bin/bash

#Saisie des programmes à tester
list="git htop tmux vim"
for element in $list 
	do
		which $element > /dev/null 2>&1
		if [ $? -eq 0 ];
		then echo -e "[...] $element : installé [...]"

		else		
		echo -e "[/!\] $element : pas installé [/!\] lancer la commande : \`apt-get install $element\`"
		fi
	done
# on créé un variable list qui contient les noms des paquets à vérifier
# Pour chaque élément dans la list, on vérifie s'il est installé via la commande which
