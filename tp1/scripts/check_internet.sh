#!/bin/bash

# CHECK INTERNET CONNECTION
echo -e "[...] Checking internet connection [...]"

# On tente de télécharger le fichier index de google
wget -q --spider www.google.fr

if [ $? -eq 0 ];
	then echo -e "[...] Internet access OK    [...]"
else
	echo -e "[/!\] Not connected to Internet   [/!\]"
	echo -e "[/!\] Please check configuration  [/!\]"
fi 

# la ligne 2 permet de télécharger le fichier index.html de google.
# l'option -q permet de ne pas afficher les sorties de la commande wget 
# l'option --spider permet de juste vérifier que l'on peut télécharger le fichier 
# $? renvoie le résultat de la commande précédente, si elle s'est bien déroulé 0 est renvoyé, sinon c'est 1 
