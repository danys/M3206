#!/bin/bash

if which ssh > /dev/null 2>&1
then	
	echo "[...] ssh: est installé [/!\]"

	if service ssh status > /dev/null 2>&1
	then
		echo "[...] ssh: fonctionne [...]"
	else
		echo "[...] ssh: le service n'est pas lancé [/!\]"
		echo "[/!\] ssh: lancement du service [/!\] lancer la commande: \`/etc/init.d/ssh\`"
	fi
else
	echo "[...] ssh: n'est pas installé [/!\]" 
fi
		

# On regarde si ssh est installé via la commande which, on redirige la sortie pour ne pas avoir d'affichage
# Si le résultat de la commande which est 0, ssh est donc installé, on check le statut
# Si le résultat est 1, ssh n'est pas installé
