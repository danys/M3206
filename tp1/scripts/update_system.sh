#!/bin/bash

# Vérification que l'on est l'utilisateur root
if [ $EUID != 0 ];
	then echo -e "[/!\] Vous devez être super-utilisateur [/!\]"
else
	echo -e "[...] update database [...]"
	apt-get update
	echo -e "[...] update system   [...]"
	apt-get upgrade
fi

# La condition sur le if est tout simplement l'ID de l'utilisateur qui lance le script, celui de root est 0, donc si l'ID est différent de 0, c'est que l'utilisateur n'est pas root, et il ne pourra donc pas lancer la mise à jour du système
