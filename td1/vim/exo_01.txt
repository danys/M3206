*temp var1 0
*temp var2 "hi"
*temp var3 -1
*temp var4 42
*temp var5 "asdf"
*temp var6 0

Simple things we do all the time should be able to be done with very few keystrokes,
but sometimes I find something I need to do makes me go, "There MUST be a better way."

This challenge is just a simple movement and entering text at a certain place.

# Combinaison de touches utilisées pour transformer le texte :
# gg -> aller à la première ligne 
# yy -> copier toute la ligne
# 6G -> aller à la sixième ligne 
# p  -> coller la ligne copiée (la première)
# W  -> se déplacer au mot suivant (var1) 
# e  -> aller sur le dernier caractère (1)
# r7 -> remplacer le caractère par "7" 
# w  -> mot suivant
# R  -> passage en mode remplacement, on écrit alors "11" puis échap pour quitter le mode
# 11G-> aller à la onzième ligne (la ligne vide en dessous de There MUST...)
# o  -> ouvrir une ligne et passage en mode insertion
# On tape le text "New text."
# touche entrée -> sauter une ligne
# echap -> retourner en mode visualisation
